#version 430 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec2 tex;
layout (location = 2) in vec3 normal;

out vec2 oTex;

uniform mat4 transform;
uniform mat4 projt;
uniform mat4 view;

void main()
{
    gl_Position = projt * view * transform * vec4(position, 1.0f);
    oTex = tex;
}
