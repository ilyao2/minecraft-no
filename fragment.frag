#version 430 core
in vec2 oTex;
in vec3 Normal;
in vec3 oPos;
in float id;

out vec4 color;

struct Material {

    sampler2D mdiffuse1;
    sampler2D mdiffuse2;
    sampler2D mdiffuse3;
    sampler2D mspecular;
};
struct Light {
    vec3 lightPos;
    vec3 ldiffuse;
    vec3 lspecular;
    vec3 lambient;

    float constant;
    float linear;
    float quadratic;
};

uniform Material material;


uniform vec3 viewPos;

uniform Light light[2];
vec3 CalcPointLight(Light light, vec3 normal, vec3 fragPos, vec3 viewDir, vec3 textureVec1, vec3 spectextureVec1);

void main()
{
    int l;
    vec3 textureVec;
    vec3 spectextureVec;
    l = int(id);
    switch (l)
    {
    case 1:
        {

            textureVec = vec3(texture(material.mdiffuse1, oTex));
            spectextureVec = vec3(texture(material.mspecular, oTex));
            break;
        }
    case 2:
        {

            textureVec = vec3(texture(material.mdiffuse2, oTex));
            spectextureVec = vec3(texture(material.mdiffuse2, oTex));
            break;
        }
    case 3:
        {
            textureVec = vec3(texture(material.mdiffuse3, oTex));
            spectextureVec = vec3(texture(material.mdiffuse3, oTex));
            break;
        }
    }

    vec3 norm = normalize(Normal);
    vec3 viewDir = normalize(viewPos - oPos);
    vec3 result;

    for(int i = 0; i < 2; i++)
        result += CalcPointLight(light[i], norm, oPos, viewDir, textureVec, spectextureVec);


    color = vec4(result, 1.0f);
}
vec3 CalcPointLight(Light light, vec3 normal, vec3 fragPos, vec3 viewDir, vec3 textureVec1, vec3 spectextureVec1)
{
    vec3 lightDir = normalize(light.lightPos - fragPos);
    // ��������� ���������
    float diff = max(dot(normal, lightDir), 0.0);
    // ��������� ���������� ������
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
    // ���������
    float distance    = length(light.lightPos - fragPos);
    float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
    // ����������� ����������
    vec3 ambient  = light.lambient  *        textureVec1;
    vec3 diffuse  = light.ldiffuse  * diff * textureVec1;
    vec3 specular = light.lspecular * spec * spectextureVec1;
    diffuse  *= attenuation;
    specular *= attenuation;
    return (ambient + diffuse + specular);
};
