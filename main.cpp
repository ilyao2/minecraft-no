#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <cmath>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <SOIL/SOIL.h>
#include "shader.h"

using namespace std;
using namespace glm;

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void do_movement();
void mouseclick (float posit[][32][32], vector <GLfloat> &vertices, GLuint& VAO, GLuint& VBO);
void intersects(float posit[][32][32]);
void addvec(float x, float y, float z, vector <GLfloat> &vertices, float id);
void loadrex(GLuint &textureid, char* path);
void sellvectors(float posit[][32][32], vector <GLfloat> &vertices, GLuint& VAO, GLuint& VBO);
void line(float positions[][32][32], int, int, int, int, int);


const GLuint WIDTH = 1920, HIGHT = 1080;      //������� ����

GLfloat lastX = WIDTH/2.0f, lastY = HIGHT/2.0f;    //���������� ��� ����
GLfloat yawp = 90.0f;
GLfloat pitchp = 0.0f;
bool firstMouse = 1;
bool jampen=0;
float jtime=0;
float qtimer=0;
bool onground = 0;
vec3 cameraPos   = glm::vec3(0.0f, 30.0f,  3.0f);   // ������
vec3 cameraFront = glm::vec3(0.0f, 0.0f, 1.0f);
vec3 cameraUp    = glm::vec3(0.0f, 1.0f,  0.0f);

bool keys[1024];                 // ������ �������� ������� ������

GLfloat deltaTime = 0.0f;	// �����, ��������� ����� ��������� � ������� ������
GLfloat lastFrame = 0.0f;  	// ����� ������ ���������� �����



int main()
{



    mat4 rotmat;
    mat4 view;

    // �������������
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RELEASE, GL_FALSE);


    GLFWwindow* window;

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(WIDTH, HIGHT, "Hello World", glfwGetPrimaryMonitor(), NULL);
    if (!window)
    {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // callback �������
    glfwSetKeyCallback(window, key_callback);
    glfwSetCursorPosCallback(window, mouse_callback);

    //�������������
    glewExperimental = GL_TRUE;
    glewInit();

    cout << glGetString(GL_VERSION);


    glViewport(0, 0, WIDTH, HIGHT);
    Shader shader1("vertex.frag", "fragment.frag");
    Shader Lshader("Lvertex.frag", "Lfragment.frag");


    vector <GLfloat> vertices;
    float positions[32][32][32];
    for (int x=0; x<32; x++)
        for (int y=0; y<32; y++)
            for (int z=0; z<32; z++)
            {
                if (y <5 || (y>-4 && x==0 && z==0) || (x==3 && y==3 && z==2) || (x==-3 && y==-2 && z==-1))
                {
                    positions[x][y][z] = 1.0f;
                }
                else if(x > 28 && y < 20 && z < 25 && z > 5)
                {
                    positions[x][y][z] = 2.0f;
                }
                else if (z > 7 && x < 2 && y < 10)
                {
                    positions[x][y][z] = 3.0f;
                }
                else
                {
                    positions[x][y][z] = 0.0f;
                }
            }
    int Fz=5, Fy=6, Lz=28,Ly=15;
    float len;
    float dz = Lz-Fz;
    float dy = Ly-Fy;
    if(abs(dz) >= abs(dy))
    {
        len=abs(dz);
    }
    else
    {
        len=abs(dy);
    }

    float Pz = (Lz-Fz)/len;
    float Py = (Ly-Fy)/len;
    float nz = Fz+0.5*sign(Pz);
    float ny = Fy+0.5*sign(Py);
    int ik = 0;
    while(ik<=len)
    {
        positions[7][(int)floor(ny)][(int)floor(nz)] = 2.0f;
        nz+=Pz;
        ny+=Py;
        ik++;
    }

    Pz = dz;
    Py = dy;
    float E = 2*Py - Pz;
    ik=Pz;
    ny=Fy;
    nz=Fz;
    positions[10][(int)ny][(int)nz] = 3.0f;
    while (--ik >= 0)
    {
        if (E >= 0)
            {
                nz= nz + 1;
                ny= ny + 1;
                E=E+2*(Py-Pz);
            }
        else
        {
            nz= nz + 1;
            E=E + 2*Py;
        }
        positions[10][(int)ny][(int)nz] = 3.0f;
    }

    float R = 8;
    nz=0; ny=R;
    float dl= 2*(1-R);
    float e;
    float lim =0;
   n1:
        positions[15][int(ny)+6][int(nz)+10] = 3.0f;
        if(ny < lim)
        {
            goto n4;
        }
        if(dl<0)
        {
            goto n2;
        }
        if(dl>0)
        {
            goto n3;
        }
        if(dl == 0)
        {
            goto n20;
        }
    n2:
        e=2*dl+2*ny-1;
        if(e <= 0)
        {
            goto n10;
        }
        if(e>0)
        {
            goto n20;
        }
    n3:
        e=2*dl-2*nz-1;
        if(e <=0)
        {
            goto n20;
        }
        if(e>0)
        {
            goto n30;
        }
    n10:
        nz++;
        dl+=2*nz+1;
        goto n1;
    n20:
        nz++;
        ny--;
        dl+=2*nz-2*ny+2;
        goto n1;
    n30:
        ny--;
        dl-=2*ny+1;
        goto n1;
    n4:





    line(positions,5,6,23,6,24);
    line(positions,23,6,20,20,24);
    line(positions,10,21,20,20,24);
    line(positions,7,18,10,21,24);
    line(positions,5,6,7,18,24);

    line(positions,5,6,16,6,20);
    line(positions,16,6,16,20,20);
    line(positions,16,20,5,20,20);
    line(positions,5,20,5,6,20);

    bool il=0;
    int iz=9;
    int iy=9;
    float filltimer = 0;


    const unsigned int SHADOW_WIDTH = 1024, SHADOW_HEIGHT = 1024;

    GLuint depthMap;
    glGenTextures(1, &depthMap);
    glBindTexture(GL_TEXTURE_2D, depthMap);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);




    GLuint VBO, VAO;

    sellvectors(positions, vertices, VBO, VAO);

    GLuint FBO;
    glGenFramebuffers(1, &FBO);
    glBindFramebuffer(GL_FRAMEBUFFER, FBO);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);


            // Load and create a texture
    GLuint texture;
    loadrex(texture, "tex.png");
    GLuint texture2;
    loadrex(texture2, "tex2.png");
    GLuint texture3;
    loadrex(texture3, "tex11.png");
    GLuint texture4;
    loadrex(texture4, "tex21.png");


    glEnable(GL_DEPTH_TEST);




    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window))
    {

        glfwPollEvents();
        do_movement();
        mouseclick(positions, vertices, VAO, VBO);
        intersects(positions);
        /* Render here */

//
//        cout << "X = " << cameraPos.x << "   Z = " << cameraPos.z << "   Y = " << cameraPos.y << endl;

        mat4 view;
        mat4 proj;


        float near_plane = 1.0f, far_plane = 7.5f;
        mat4 lightProjection = ortho(-10.0f, 10.0f, -10.0f, 10.0f, near_plane, far_plane);

        mat4 lightView = lookAt(vec3(-2.0f, 4.0f, -1.0f),
                                vec3( 0.0f, 0.0f,  0.0f),
                                vec3( 0.0f, 1.0f,  0.0f));
        mat4 lightSpaceMatrix = lightProjection * lightView;

        view = lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
        proj = perspective( 45.0f, (float)WIDTH/(float)HIGHT, 0.1f, 50.0f);

        GLfloat currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;
        if (qtimer < 0.6)
        qtimer += deltaTime;
        filltimer += deltaTime;

        if(!il && filltimer > 0.2)
        {
            filltimer = 0;
            positions[20][iy][iz] = 3.0f;
            sellvectors(positions, vertices, VAO, VBO);
            if(positions[20][iy-1][iz] == 0.0f)
            {
                iy--;
            }
            else if(positions[20][iy][iz-1] == 0.0f)
            {
                iz--;
            }
            else if(positions[20][iy+1][iz] == 0.0f)
            {
                iy++;
            }
            else if(positions[20][iy][iz+1] == 0.0f)
            {
                iz++;
            }
            else
            {
                il=1;
            }
        }


        shader1.use();
        GLuint transformLoc = glGetUniformLocation(shader1.Program, "transform");

        GLuint projLoc = glGetUniformLocation(shader1.Program, "projt");
        glUniformMatrix4fv(projLoc, 1, GL_FALSE, value_ptr(proj));

        GLuint viewLoc = glGetUniformLocation(shader1.Program, "view");
        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, value_ptr(view));

        GLint lightColorLoc  = glGetUniformLocation(shader1.Program, "light[0].ldiffuse");
        glUniform3f(lightColorLoc,  1.0f, 1.0f, 1.0f); // ������� ���� ��������� ����� (�����)
        GLint lightSpecLoc  = glGetUniformLocation(shader1.Program, "light[0].lspecular");
        glUniform3f(lightSpecLoc,  1.0f, 1.0f, 1.0f);
        GLint lightAmbLoc  = glGetUniformLocation(shader1.Program, "light[0].lambient");
        glUniform3f(lightAmbLoc,  0.03f, 0.03f, 0.03f);
        GLint lightConLoc  = glGetUniformLocation(shader1.Program, "light[0].constant");
        glUniform1f(lightConLoc,  1.0f);
        GLint lightLinLoc  = glGetUniformLocation(shader1.Program, "light[0].linear");
        glUniform1f(lightLinLoc,  0.045f);
        GLint lightQuaLoc  = glGetUniformLocation(shader1.Program, "light[0].quadratic");
        glUniform1f(lightQuaLoc,  0.0075f);
        GLint lightPosLoc  = glGetUniformLocation(shader1.Program, "light[0].lightPos");
        glUniform3f(lightPosLoc, 2.0f, 20.0f, 20.0f);


        lightColorLoc  = glGetUniformLocation(shader1.Program, "light[1].ldiffuse");
        glUniform3f(lightColorLoc,  1.0f, 0.8f, 0.0f); // ������� ���� ��������� ����� (�����)
        lightSpecLoc  = glGetUniformLocation(shader1.Program, "light[1].lspecular");
        glUniform3f(lightSpecLoc,  1.0f, 0.8f, 0.0f);
        lightAmbLoc  = glGetUniformLocation(shader1.Program, "light[1].lambient");
        glUniform3f(lightAmbLoc,  0.03f, 0.03f, 0.03f);
        lightConLoc  = glGetUniformLocation(shader1.Program, "light[1].constant");
        glUniform1f(lightConLoc,  1.0f);
        lightLinLoc  = glGetUniformLocation(shader1.Program, "light[1].linear");
        glUniform1f(lightLinLoc,  0.045f);
        lightQuaLoc  = glGetUniformLocation(shader1.Program, "light[1].quadratic");
        glUniform1f(lightQuaLoc,  0.0075f);
        lightPosLoc  = glGetUniformLocation(shader1.Program, "light[1].lightPos");
        glUniform3f(lightPosLoc, 2.0f, 9.0f, -2.0f);

        GLuint lightSpaceMatrixLoc = glGetUniformLocation(shader1.Program, "lightSpaceMatrix");
        glUniformMatrix4fv (lightSpaceMatrixLoc,1,GL_FALSE, value_ptr(lightSpaceMatrix));




        GLint viewPosLoc = glGetUniformLocation(shader1.Program, "viewPos");
        glUniform3f(viewPosLoc, cameraPos.x, cameraPos.y, cameraPos.z);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture);
        glUniform1i(glGetUniformLocation(shader1.Program, "material.mdiffuse1"), 0);

        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, texture3);
        glUniform1i(glGetUniformLocation(shader1.Program, "material.mdiffuse2"), 2);

        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, texture4);
        glUniform1i(glGetUniformLocation(shader1.Program, "material.mdiffuse3"), 3);

        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, texture2);
        glUniform1i(glGetUniformLocation(shader1.Program, "material.mspecular"), 1);

//
//        Lshader.use();
//        projLoc = glGetUniformLocation(Lshader.Program, "projt");
//        glUniformMatrix4fv(projLoc, 1, GL_FALSE, value_ptr(proj));
//        viewLoc = glGetUniformLocation(Lshader.Program, "view");
//        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, value_ptr(view));



//        // 1. first render to depth map
//glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
//glBindFramebuffer(GL_FRAMEBUFFER, FBO);
//    glClear(GL_DEPTH_BUFFER_BIT);
//      glBindVertexArray(VAO);
//        mat4 model;
//        glUniformMatrix4fv(transformLoc, 1, GL_FALSE, value_ptr(model));
//        glDrawArrays(GL_TRIANGLES, 0, vertices.size()/9);
//        glBindVertexArray(0);
//        /* Swap front and back buffers */
//        glfwSwapBuffers(window);
//glBindFramebuffer(GL_FRAMEBUFFER, 0);
// 2. then render scene as normal with shadow mapping (using depth map)
        glViewport(0, 0, WIDTH, HIGHT);
        glClearColor(0.8f, 1.0f, 1.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glBindVertexArray(VAO);
        mat4 model;
        glUniformMatrix4fv(transformLoc, 1, GL_FALSE, value_ptr(model));
        glDrawArrays(GL_TRIANGLES, 0, vertices.size()/9);
        glBindVertexArray(0);
        /* Swap front and back buffers */
        glfwSwapBuffers(window);
//        glBindTexture(GL_TEXTURE_2D, depthMap);

//        glClearColor(0.8f, 1.0f, 1.0f, 1.0f);
//        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//        glBindVertexArray(VAO);
//        mat4 model;
//        glUniformMatrix4fv(transformLoc, 1, GL_FALSE, value_ptr(model));
//        glDrawArrays(GL_TRIANGLES, 0, vertices.size()/9);
//        glBindVertexArray(0);
//        /* Swap front and back buffers */
//        glfwSwapBuffers(window);



    }

    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteTextures(1, &texture);
    glDeleteTextures(1, &texture2);
    glDeleteTextures(1, &texture3);
    glfwTerminate();
    return 0;
}












void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(window, GL_TRUE);
    }
    if (key >= 0 && key < 1024)
    {
        if (action == GLFW_PRESS)
            keys[key] = true;
        else if (action == GLFW_RELEASE)
            keys[key] = false;


    }
};
void do_movement()
{
    // Camera controls

    GLfloat cameraSpeed =  5.0f*deltaTime;
    if (keys[GLFW_KEY_W])
        cameraPos += cameraSpeed * normalize(cameraFront * vec3(1.0f, 0.0f, 1.0f));
    if (keys[GLFW_KEY_S])
        cameraPos -= cameraSpeed * normalize(cameraFront * vec3(1.0f, 0.0f, 1.0f));
    if (keys[GLFW_KEY_A])
        cameraPos -= normalize(cross(cameraFront, cameraUp)) * cameraSpeed;
    if (keys[GLFW_KEY_D])
        cameraPos += normalize(cross(cameraFront, cameraUp)) * cameraSpeed;
    if (keys[GLFW_KEY_SPACE] && jampen == 0 && onground == 1)
    {
        jampen = 1;
        jtime = 0;
        onground = 0;
    }

    if (jtime < 0.3 && jampen == 1)
    {
        jtime += deltaTime;
        cameraPos += 2*cameraSpeed * cameraUp;
    }
    else
    {
        jampen = 0;
    }
};
void intersects(float posit[][32][32])
{
    GLfloat cameraSpeed =  5.0f*deltaTime;
    if(!jampen)
    if (posit[int(round(cameraPos.x))][int(round(cameraPos.y-1.9))][int(round(cameraPos.z))] == 0.0f)
    {
        cameraPos -= 2*cameraSpeed * cameraUp;
        onground = 0;
    }
    else
    {
        cameraPos.y = round(cameraPos.y-1.9)+1.9;
        onground = 1;
    }
    if(!onground)
    if (posit[int(round(cameraPos.x))][int(round(cameraPos.y+0.20))][int(round(cameraPos.z))] != 0.0f)
    {
        cameraPos.y = round(cameraPos.y+0.2)-0.70;
        jampen = 0;
    }

    if (posit[int(round(cameraPos.x+0.20))][int(round(cameraPos.y))][int(round(cameraPos.z))] != 0.0f || posit[int(round(cameraPos.x+0.20))][int(round((cameraPos.y)-0.75))][int(round(cameraPos.z))] != 0.0f)
    {
        cameraPos.x = round(cameraPos.x+0.2)-0.70;
    }

    if (posit[int(round(cameraPos.x-0.20))][int(round(cameraPos.y))][int(round(cameraPos.z))] != 0.0f || posit[int(round(cameraPos.x-0.20))][int(round((cameraPos.y)-0.75))][int(round(cameraPos.z))] != 0.0f)
    {
        cameraPos.x = round(cameraPos.x-0.2)+0.70;
    }

    if (posit[int(round(cameraPos.x))][int(round(cameraPos.y))][int(round(cameraPos.z+0.20))] != 0.0f || posit[int(round(cameraPos.x))][int(round((cameraPos.y)-0.75))][int(round(cameraPos.z+0.20))] != 0.0f)
    {
        cameraPos.z = round(cameraPos.z+0.2)-0.70;
    }

    if (posit[int(round(cameraPos.x))][int(round(cameraPos.y))][int(round(cameraPos.z-0.20))] != 0.0f || posit[int(round(cameraPos.x))][int(round((cameraPos.y)-0.75))][int(round(cameraPos.z-0.20))] != 0.0f)
    {
        cameraPos.z = round(cameraPos.z-0.2)+0.70;
    }

};
void mouseclick(float posit[][32][32], vector <GLfloat> &vertices, GLuint& VAO, GLuint& VBO)
{
    int x,y,z;
    if (keys[GLFW_KEY_E] && qtimer > 0.5)
    {
        x = round(cameraPos.x + cameraFront.x);
        y = round(cameraPos.y + cameraFront.y);
        z = round(cameraPos.z + cameraFront.z);
        if (posit[x][y][z] != 0.0f)
        {
            qtimer = 0;
            posit[x][y][z] = 0.0f;
            sellvectors(posit, vertices, VAO, VBO);
        }
        if(qtimer > 0.5)
        {
            x = round(cameraPos.x + 2*cameraFront.x);
            y = round(cameraPos.y + 2*cameraFront.y);
            z = round(cameraPos.z + 2*cameraFront.z);
            if (posit[x][y][z] != 0.0f)
            {
                qtimer = 0;
                posit[x][y][z] = 0.0f;
                sellvectors(posit, vertices, VAO, VBO);
            }
        }
        if(qtimer > 0.5)
        {
            x = round(cameraPos.x + 3*cameraFront.x);
            y = round(cameraPos.y + 3*cameraFront.y);
            z = round(cameraPos.z + 3*cameraFront.z);
            if (posit[x][y][z] != 0.0f)
            {
                qtimer = 0;
                posit[x][y][z] = 0.0f;
                sellvectors(posit, vertices, VAO, VBO);
            }
        }

    }
    if (keys[GLFW_KEY_Q] && qtimer > 0.5)
    {
        x = round(cameraPos.x + cameraFront.x);
        y = round(cameraPos.y + cameraFront.y);
        z = round(cameraPos.z + cameraFront.z);
            if(posit[x][y][z] == 0.0f)
            {
                x = round(cameraPos.x + 2*cameraFront.x);
                y = round(cameraPos.y + 2*cameraFront.y);
                z = round(cameraPos.z + 2*cameraFront.z);
                if(posit [x][y][z] != 0.0f)
                {
                    x = round(cameraPos.x + cameraFront.x);
                    y = round(cameraPos.y + cameraFront.y);
                    z = round(cameraPos.z + cameraFront.z);
                    qtimer = 0;
                    posit[x][y][z] = 2.0f;
                    sellvectors(posit, vertices, VAO, VBO);
                }


                x = round(cameraPos.x + 2*cameraFront.x);
                y = round(cameraPos.y + 2*cameraFront.y);
                z = round(cameraPos.z + 2*cameraFront.z);
                if(posit[x][y][z] == 0.0f && qtimer > 0.5)
                {
                    x = round(cameraPos.x + 3*cameraFront.x);
                    y = round(cameraPos.y + 3*cameraFront.y);
                    z = round(cameraPos.z + 3*cameraFront.z);
                    if(posit [x][y][z] != 0.0f)
                    {
                        x = round(cameraPos.x + 2*cameraFront.x);
                        y = round(cameraPos.y + 2*cameraFront.y);
                        z = round(cameraPos.z + 2*cameraFront.z);
                        qtimer = 0;
                        posit[x][y][z] = 2.0f;
                        sellvectors(posit, vertices, VAO, VBO);
                    }

                    x = round(cameraPos.x + 3*cameraFront.x);
                    y = round(cameraPos.y + 3*cameraFront.y);
                    z = round(cameraPos.z + 3*cameraFront.z);
                    if(posit[x][y][z] == 0.0f && qtimer > 0.5)
                    {
                        x = round(cameraPos.x + 4*cameraFront.x);
                        y = round(cameraPos.y + 4*cameraFront.y);
                        z = round(cameraPos.z + 4*cameraFront.z);
                        if(posit [x][y][z] != 0.0f)
                        {
                            x = round(cameraPos.x + 3*cameraFront.x);
                            y = round(cameraPos.y + 3*cameraFront.y);
                            z = round(cameraPos.z + 3*cameraFront.z);
                            qtimer=0;
                            posit[x][y][z] = 2.0f;
                            sellvectors(posit, vertices, VAO, VBO);
                        }
                    }
                }
            }
    }
};

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    if (firstMouse) // ��� ���������� ���� ������������������� ��������� true
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }
    GLfloat xoffset = xpos - lastX;
    GLfloat yoffset = lastY - ypos; // �������� ������� ��������� ������ ��� ������� Y-���������� ���������� � ����� ����
    lastX = xpos;
    lastY = ypos;

    GLfloat sensitivity = 0.10f;
    xoffset *= sensitivity;
    yoffset *= sensitivity;

    yawp += xoffset;
    pitchp += yoffset;
    if(pitchp > 89.0f)
        pitchp =  89.0f;
    if(pitchp < -89.0f)
        pitchp = -89.0f;

    vec3 front;
    front.x = cos(radians(yawp)) * cos(radians(pitchp));
    front.y = sin(radians(pitchp));
    front.z = sin(radians(yawp)) * cos(radians(pitchp));
    cameraFront = normalize(front);
};
void addvec(float x, float y, float z, vector <GLfloat> &vertices, float id)
{
    vertices.push_back(x-0.5f);        vertices.push_back(y-0.5f);        vertices.push_back(z-0.5f);     vertices.push_back(0.0f);       vertices.push_back(0.0f);  vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back(-1.0f); vertices.push_back(id);
    vertices.push_back(x+0.5f);         vertices.push_back(y-0.5f);          vertices.push_back(z-0.5f);       vertices.push_back(1.0f);         vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back(-1.0f); vertices.push_back(id);
    vertices.push_back(x+0.5f);         vertices.push_back(y+0.5f);          vertices.push_back(z-0.5f);       vertices.push_back(1.0f);         vertices.push_back(1.0f); vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back(-1.0f); vertices.push_back(id);
    vertices.push_back(x+0.5f);         vertices.push_back(y+0.5f);          vertices.push_back(z-0.5f);       vertices.push_back(1.0f);         vertices.push_back(1.0f); vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back(-1.0f); vertices.push_back(id);
    vertices.push_back(x-0.5f);         vertices.push_back(y+0.5f);          vertices.push_back(z-0.5f);       vertices.push_back(0.0f);         vertices.push_back(1.0f); vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back(-1.0f); vertices.push_back(id);
    vertices.push_back(x-0.5f);         vertices.push_back(y-0.5f);          vertices.push_back(z-0.5f);       vertices.push_back(0.0f);         vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back(-1.0f); vertices.push_back(id);

    vertices.push_back(x-0.5f);         vertices.push_back(y-0.5f);          vertices.push_back(z+0.5f);       vertices.push_back(0.0f);         vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back( 1.0f); vertices.push_back(id);
    vertices.push_back(x+0.5f);         vertices.push_back(y-0.5f);          vertices.push_back(z+0.5f);       vertices.push_back(1.0f);         vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back( 1.0f); vertices.push_back(id);
    vertices.push_back(x+0.5f);         vertices.push_back(y+0.5f);          vertices.push_back(z+0.5f);       vertices.push_back(1.0f);         vertices.push_back(1.0f); vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back( 1.0f); vertices.push_back(id);
    vertices.push_back(x+0.5f);         vertices.push_back(y+0.5f);          vertices.push_back(z+0.5f);       vertices.push_back(1.0f);         vertices.push_back(1.0f); vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back( 1.0f); vertices.push_back(id);
    vertices.push_back(x-0.5f);         vertices.push_back(y+0.5f);          vertices.push_back(z+0.5f);       vertices.push_back(0.0f);         vertices.push_back(1.0f); vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back( 1.0f); vertices.push_back(id);
    vertices.push_back(x-0.5f);         vertices.push_back(y-0.5f);          vertices.push_back(z+0.5f);       vertices.push_back(0.0f);         vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back( 1.0f); vertices.push_back(id);

    vertices.push_back(x-0.5f);          vertices.push_back(y+0.5f);          vertices.push_back(z+0.5f);       vertices.push_back(1.0f);         vertices.push_back(0.0f); vertices.push_back(-1.0f); vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back(id);
    vertices.push_back(x-0.5f);          vertices.push_back(y+0.5f);          vertices.push_back(z-0.5f);       vertices.push_back(1.0f);         vertices.push_back(1.0f); vertices.push_back(-1.0f); vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back(id);
    vertices.push_back(x-0.5f);          vertices.push_back(y-0.5f);          vertices.push_back(z-0.5f);       vertices.push_back(0.0f);         vertices.push_back(1.0f); vertices.push_back(-1.0f); vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back(id);
    vertices.push_back(x-0.5f);          vertices.push_back(y-0.5f);          vertices.push_back(z-0.5f);       vertices.push_back(0.0f);         vertices.push_back(1.0f); vertices.push_back(-1.0f); vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back(id);
    vertices.push_back(x-0.5f);          vertices.push_back(y-0.5f);          vertices.push_back(z+0.5f);       vertices.push_back(0.0f);         vertices.push_back(0.0f); vertices.push_back(-1.0f); vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back(id);
    vertices.push_back(x-0.5f);          vertices.push_back(y+0.5f);          vertices.push_back(z+0.5f);       vertices.push_back(1.0f);         vertices.push_back(0.0f); vertices.push_back(-1.0f); vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back(id);

    vertices.push_back(x+0.5f);          vertices.push_back(y+0.5f);          vertices.push_back(z+0.5f);       vertices.push_back(1.0f);         vertices.push_back(0.0f); vertices.push_back(1.0f); vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back(id);
    vertices.push_back(x+0.5f);          vertices.push_back(y+0.5f);          vertices.push_back(z-0.5f);       vertices.push_back(1.0f);         vertices.push_back(1.0f); vertices.push_back(1.0f); vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back(id);
    vertices.push_back(x+0.5f);          vertices.push_back(y-0.5f);          vertices.push_back(z-0.5f);       vertices.push_back(0.0f);         vertices.push_back(1.0f); vertices.push_back(1.0f); vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back(id);
    vertices.push_back(x+0.5f);          vertices.push_back(y-0.5f);          vertices.push_back(z-0.5f);       vertices.push_back(0.0f);         vertices.push_back(1.0f); vertices.push_back(1.0f); vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back(id);
    vertices.push_back(x+0.5f);          vertices.push_back(y-0.5f);          vertices.push_back(z+0.5f);       vertices.push_back(0.0f);         vertices.push_back(0.0f); vertices.push_back(1.0f); vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back(id);
    vertices.push_back(x+0.5f);          vertices.push_back(y+0.5f);          vertices.push_back(z+0.5f);       vertices.push_back(1.0f);         vertices.push_back(0.0f); vertices.push_back(1.0f); vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back(id);

    vertices.push_back(x-0.5f);          vertices.push_back(y-0.5f);          vertices.push_back(z-0.5f);       vertices.push_back(0.0f);         vertices.push_back(1.0f); vertices.push_back(0.0f); vertices.push_back(-1.0f); vertices.push_back(0.0f); vertices.push_back(id);
    vertices.push_back(x+0.5f);          vertices.push_back(y-0.5f);          vertices.push_back(z-0.5f);       vertices.push_back(1.0f);         vertices.push_back(1.0f); vertices.push_back(0.0f); vertices.push_back(-1.0f); vertices.push_back(0.0f); vertices.push_back(id);
    vertices.push_back(x+0.5f);          vertices.push_back(y-0.5f);          vertices.push_back(z+0.5f);       vertices.push_back(1.0f);         vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back(-1.0f); vertices.push_back(0.0f); vertices.push_back(id);
    vertices.push_back(x+0.5f);          vertices.push_back(y-0.5f);          vertices.push_back(z+0.5f);       vertices.push_back(1.0f);         vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back(-1.0f); vertices.push_back(0.0f); vertices.push_back(id);
    vertices.push_back(x-0.5f);          vertices.push_back(y-0.5f);          vertices.push_back(z+0.5f);       vertices.push_back(0.0f);         vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back(-1.0f); vertices.push_back(0.0f); vertices.push_back(id);
    vertices.push_back(x-0.5f);          vertices.push_back(y-0.5f);          vertices.push_back(z-0.5f);       vertices.push_back(0.0f);         vertices.push_back(1.0f); vertices.push_back(0.0f); vertices.push_back(-1.0f); vertices.push_back(0.0f); vertices.push_back(id);

    vertices.push_back(x-0.5f);          vertices.push_back(y+0.5f);          vertices.push_back(z-0.5f);       vertices.push_back(0.0f);         vertices.push_back(1.0f); vertices.push_back(0.0f); vertices.push_back(1.0f); vertices.push_back(0.0f); vertices.push_back(id);
    vertices.push_back(x+0.5f);          vertices.push_back(y+0.5f);          vertices.push_back(z-0.5f);       vertices.push_back(1.0f);         vertices.push_back(1.0f); vertices.push_back(0.0f); vertices.push_back(1.0f); vertices.push_back(0.0f); vertices.push_back(id);
    vertices.push_back(x+0.5f);          vertices.push_back(y+0.5f);          vertices.push_back(z+0.5f);       vertices.push_back(1.0f);         vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back(1.0f); vertices.push_back(0.0f); vertices.push_back(id);
    vertices.push_back(x+0.5f);          vertices.push_back(y+0.5f);          vertices.push_back(z+0.5f);       vertices.push_back(1.0f);         vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back(1.0f); vertices.push_back(0.0f); vertices.push_back(id);
    vertices.push_back(x-0.5f);          vertices.push_back(y+0.5f);          vertices.push_back(z+0.5f);       vertices.push_back(0.0f);         vertices.push_back(0.0f); vertices.push_back(0.0f); vertices.push_back(1.0f); vertices.push_back(0.0f); vertices.push_back(id);
    vertices.push_back(x-0.5f);          vertices.push_back(y+0.5f);          vertices.push_back(z-0.5f);       vertices.push_back(0.0f);         vertices.push_back(1.0f); vertices.push_back(0.0f); vertices.push_back(1.0f); vertices.push_back(0.0f); vertices.push_back(id);


};

void sellvectors(float posit[][32][32], vector <GLfloat> &vertices, GLuint& VAO, GLuint& VBO)
{
    vertices.clear();
    for (int x=0; x<32; x++)
        for (int y=0; y<32; y++)
            for (int z=0; z<32; z++)
            {
                if( posit[x][y][z] != 0.0f)
                {
                    addvec(float(x),float(y),float(z), vertices, posit[x][y][z]);
                }
            }

    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);/*
    glGenBuffers(1, &EBO);*/

    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER,VBO);

    glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(GLfloat), vertices.data(), GL_DYNAMIC_DRAW);
/*
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(index), index, GL_DYNAMIC_DRAW);*/

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9*sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 9*sizeof(GLfloat), (GLvoid*)(3*sizeof(GLfloat)));
    glEnableVertexAttribArray(1);

    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 9*sizeof(GLfloat), (GLvoid*)(5*sizeof(GLfloat)));
    glEnableVertexAttribArray(2);

    glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, 9*sizeof(GLfloat), (GLvoid*)(8*sizeof(GLfloat)));
    glEnableVertexAttribArray(3);

    glBindVertexArray(0);


};
void loadrex(GLuint &textureid, char* path)
{
    textureid;
    glGenTextures(1, &textureid);
    glBindTexture(GL_TEXTURE_2D, textureid); // All upcoming GL_TEXTURE_2D operations now have effect on this texture object
    // Set the texture wrapping parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// Set texture wrapping to GL_REPEAT (usually basic wrapping method)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    // Set texture filtering parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // Load image, create texture and generate mipmaps
    int twidth, theight;
    unsigned char* image = SOIL_load_image(path, &twidth, &theight, 0, SOIL_LOAD_RGB);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, twidth, theight, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    glReadPixels(0, 0, twidth, theight, GL_COLOR_INDEX, GL_UNSIGNED_BYTE, image);

    glGenerateMipmap(GL_TEXTURE_2D);
    SOIL_free_image_data(image);
    glBindTexture(GL_TEXTURE_2D, 0); // Unbind texture when done, so we won't accidentily mess up our texture.
};
void line(float positions[][32][32], int Fz, int Fy, int Lz, int Ly , int x)
{
    float len;
    float dz = Lz-Fz;
    float dy = Ly-Fy;
    if(abs(dz) >= abs(dy))
    {
        len=abs(dz);
    }
    else
    {
        len=abs(dy);
    }

    float Pz = (Lz-Fz)/len;
    float Py = (Ly-Fy)/len;
    float nz = Fz+0.5*sign(Pz);
    float ny = Fy+0.5*sign(Py);
    int ik = 1;
    while(ik<=len)
    {
        positions[x][(int)floor(ny)][(int)floor(nz)] = 2.0f;
        nz+=Pz;
        ny+=Py;
        ik++;
    }
}

