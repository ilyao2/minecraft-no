#version 430 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec2 tex;
layout (location = 2) in vec3 normal;
layout (location = 3) in float ID;

out vec2 oTex;
out vec3 Normal;
out vec3 oPos;
out vec4 conv;
out float id;

uniform mat4 transform;
uniform mat4 projt;
uniform mat4 view;
uniform mat4 lightSpaceMatrix;

void main()
{
    gl_Position =  projt * view * transform * vec4(position, 1.0f);
    oTex = tex;
    Normal = mat3(transpose(inverse(transform))) * normal;
    conv = transform * vec4(position, 1.0f);
    oPos = (conv.xyz);
    id = ID;
}
